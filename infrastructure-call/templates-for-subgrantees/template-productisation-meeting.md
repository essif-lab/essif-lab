# Template for Deliverable for the Productisation meeting of <!--your project>

<!--GENERAL ADVISE: Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others. -->

Contact person details: 

<!--
- name of the person to contact
-ways to contact this person (email, git, slack, others...)
--> 

## PRODUCT PAGE
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->

### 1. Define your product component(s)

The purpose of this section is to give readers a good idea of what you will be providing, so that they determine what you will be providing and whether or not that addresses their needs. So for each component that you will be providing, state:
- its name (so that we may refer to it)
- its function (one or two lines, and if possible, use the functions as defined in the eSSIF-Lab functional architecture)
- for every interface (API, protocol, ...) the component provides:
  - its name
  - its function
- any functionality that the component needs (to be provided by other components, which you or others may provide).

### 2. Fit for Purpose

The purpose of this section is to give readers an idea of who you think should be using your components, and what benefits such users will reap by doing so. To this end, state:
- the roles that you envisage users to perform as they use your component(s), and for each of them:
  - the functions they will use in that role;
  - the benefits they reap when using such functions in that role. 

### 3. Planning

The purpose of this section is to give readers an idea of when they may expect the functionalities you intend to implement become available in some form. To this end, state:
- dates that will serve as a deadline for you, and for each of them:
  - the component(s) that will have been delivered by that date;
  - the maturity-level of the delivered component(s), e.g. 'prototype', 'an indication of the maturity of  level of 

## VISION, ARCHITECTURE, COLLABORATION and INTEROP PAGE
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->

### 4. Parties/Components interop
The purpose of this section is to give readers an idea of the possibilities and limitations of the kind(s) of interop you envisage. To this end, state the parties c.q. open source components that you can interop with, either because you need their functionalities (or they need yours), or you can somehow connect your and their functionalities. Please distinguish between:
  - parties/components within eSSIF-Lab (subgrantees of the infrastructure call, subgrantees of business calls);
  - parties/components that are used within EBSI/ESSIF;
  - other parties/components

### 5. Demo
State the kind(s) of demo's that you will be providing and/or participating in to show that the interop as specified in the previous section actually works. 

### 6. eSSIF-Lab Vision, Architecture, Terminology, interfaces, and further Documentation
At the end of the project, a vision, architecture, terminology, interfaces (APIs, protocols, ...) and other documentation should exist that results from a collaboration of the subgrantees and the project team. Currently, there is a draft vision and functional architecture that gives the readers a high-level idea of what the eSSIF-Lab infrastructure is about, and how it can be used in a business. Also, some terminology has been provisionally defined. There is little API and further documentation.

In order for this overall project deliverable to be delivered, state:
- what you think is needed (short, to the point)
- what/how/where you see your team contribute
- the way(s) you prefer to work with other subgrantees to do so. 

## DEVELOPMENT PAGE
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->
The purpose of this page is to collect all ideas/preferences/... of subgrantees that allow 
- the construction and maintenance of CD/CI on GRNET infrastructure;
- the maintenance of documentation
- the provisioning of repo's (if necessary)
- ... (whatever else...)

The template-contents of this section is to be provided by GRNET.


