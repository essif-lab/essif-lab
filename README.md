# eSSIF-Lab Project

This repository contains files that subgrantees/subgrantees of the eSSIF-Lab open calls:
- need, in order to be maximally aware of what other subgrantees are doing, enabling their solutions to become optimally interoperable.
- create, in order to satisfy the aforementioned needs of the other subgrantees, and
- create, in order to satisfy eSSIF-Lab overall project requirements, as (will be) stated in this document.

Also, this repository contains some guidance for applicants to any of the eSSIF-Lab open calls.

## README for Applicants

Applicants are expected to be already active in the SSI field and to be familiar with commonly used terminology, such as “verifiable credential”, “wallet”, “issuer”, “holder” and “verifier”. Applicants are also expected to direct their work so as to realize the various benefits, and counter the risks associated with SSI, as summarized e.g. in the blog “[Self-Sovereign Identity - the good, the bad and the ugly](https://blockchain.tno.nl/blog/self-sovereign-identity-the-good-the-bad-and-the-ugly/)”.

Subgrantees are expected to work in an ecosystem with other subgrantees for the purpose of ensuring interoperability at the various levels (technology, processes, business/policy), scalability of solutions, and ensuring that solutions are fit-for-purpose (i.e. can actually be used in practice). This implies that subgrantees collaborate in the project to maintain a shared vision, functional architecture and specifications of functionalities, API’s, ways of working etc.

The eSSIF-Lab consortium has drafted:

- an initial [Vision and Purpose document](https://essif-lab.github.io/framework/docs/vision-and-purpose), which states at a high level what it is eSSIF-Lab aims to contribute in terms of impact that it wants to see realized.
- an initial [Functional Architecture document](https://essif-lab.github.io/framework/docs/functional-architecture/), which identifies several functional components that sit on top of different kinds of SSI-technologies in order to realize the vision, purpose and impact. This document will be updated as necessary throughout the project.

Proposals may be submitted to any of [three calls](https://essif-lab.eu/open-calls/):

1. The [Infrastructure-oriented Call](https://essif-lab-infrastructure-oriented.fundingbox.com/), which is **currently (still) open**, requests proposals for work in the [SSI-Roles layer](https://essif-lab.github.io/framework/docs/functional-architecture/#3--essif-lab-infrastructure-functional-components), and/or work in the SSI Protocols and Crypto layer if that is necessary for establishing/improving interoperability, scalability or features that provide generic business benefits. There is a [guide for applicants](https://s3.amazonaws.com/fundingbox-sites/gear%2F1587280828747-eSSIF-Lab_GuideforApplicants_IOC_Updatedv.3.pdf), and a [FAQ](https://s3.amazonaws.com/fundingbox-sites/gear%2F1587280828747-eSSIF-Lab_GuideforApplicants_IOC_Updatedv.3.pdf) for this call.

2. The [First Business-oriented Call](https://essif-lab-first-business-oriented.fundingbox.com/), for which the **application deadline has passed**, called for proposals that extend the eSSIF-Lab basic infrastructure/architecture with business solutions that makes it easy for organizations to deploy and/or use SSI, reduce business risks, facilitate alignment of business information, etc.

3. The [Second Business-oriented call](https://essif-lab-second-business-oriented.fundingbox.com/), which is **expected to launch in late spring 2021**, will be open to SMEs and startups developing commercial SSI-based applications and services with focus in the verticals of HealthTech, eGovernment, Education or competing in the generic track of Open Disruptive Innovation.

## README for subgrantees.

Subgrantees are the parties that have signed a sub-grant agreement for a project under one of the calls. For each of the calls, we have a README in the directory that contains the material for its subgrantees, as follows:

- [Business Open Call #1 README](./business-open-call-1/README.md)
- [Infrastructure Call README](./infrastructure-call/README.md)
- [Business Open Call #2 README](./business-open-call-2/README.md)

---