# <!--subproject name-->: Contribution To eSSIF-Lab Vision and Framework

For more information, please contact:

<!--
- name of the person to contact
- ways to contact this person (email, git, slack, others...)
-->

<!--This is a template that can be used to describe the contribution that the project makes to the SSI and eSSIF-Lab vision and framework, for which the deadline is Monday 7 September EOB.

Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others.

Be mindful of the audience you target, which are primarily project experts that evaluate your work, but there may be other audiences as well.-->

## Summary

This document describes the contributions that this project makes to the [eSSIF-Lab Vision and Framework](https://essif-lab.github.io/framework/). <!--Summarize the contributions you have made, and plan to make, appropriately referencing the parts of the vision/framework.-->

## Contributions for eSSIF-Lab goals

<!--Concisely describe if/how your solution contributes to reduce

- administrative burdens for citizens and businesses, e.g. through automated filling in of forms, and/or support of automated issuing of missing pieces of data.
- data verification and decision costs for government and organisations, e.g. through automated requests for semantically specified data, and/or automated decision making
- ... (other business issues)
-->

## Contributions to symmetry in business transactions

<!--Concisely describe if how your solution contributes to empower individuals to become as potent as businesses, i.e. enabling all parties to dynamically switch between holder, verifier and issuers roles during a  single transaction. Examples include

- holder wallets that also support verifier and issuer functionality.
- server systems that also support holder/presentation and issuer functionality.
- solutions to protect citizens against unscrupulous data guzzlers.
- (Counterexample: cookie walls – accept my cookies or go into the maze)
-->

## Interoperability through the use of standards

<!-- Concisely describe if/how
- Which **SSI standards** does/will your solution implement?
- What other parties/systems would your solution interoperate with?
- What are the specific interoperability points (protocols, data models, APIs, …)?
- What types of interop testing would be relevant for your solution?
- What SSI standards relevant to your solution are still missing or incomplete?
- How and what would you contribute to create/complete such standards?
- How would such contributions benefit your own business proposition, as well as that of others in the SSI business ecosystem?
- How will you leverage StandICT.eu or other grants for this?
-->

## Synergies

<!--Which **synergies / added values** do you foresee in the SSI business ecosystem?
- what is the added value of your solution to the SSI business ecosystem?
- Who are your customers? How do you reach them? What would they be they paying for? How are they paying?
- What other parties would provide added value to your positions in the SSI business ecosystem?
- Who are your suppliers and/or partners? How do your envision the value exchange with them?
- How do these added values support the economic and other types of sustainability of an SSI business ecosystem?
-->

## Review of the eSSIF-Lab Framework
<!--
The eSSIF-Lab project requires that subgrantees adhere to the [eSSIF-Lab Framework](https://essif-lab.github.io/framework/) (its [Vision](https://essif-lab.github.io/framework/docs/vision-and-purpose), [Functional Architecture](https://essif-lab.github.io/framework/docs/functional-architecture) and whatever is further developed in this project), or propose additions/modifications to better support its purposes. Please describe:

- How clear are the purpose, target audience and message?
- What are strengths of what you read? What are weaknesses?
- What aspects could be concretely improved?
- How would your solution fit in this, or made to fit?
(Please use “Dutch directness” in your review, we appreciate that 😊).
-->
