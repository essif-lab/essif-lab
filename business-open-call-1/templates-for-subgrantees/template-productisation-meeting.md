# Template for Deliverable for the Pruductisation meeting of <!--your project>

<!--GENERAL ADVISE: Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others. -->

Contact person details: 

<!--
- name of the person to contact
-ways to contact this person (email, git, slack, others...)
--> 

## Business Page
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->

### 1. Define your product 

### 2. Value proposition – what is/are the benefit(s) for your customer

### 3. Who are your customers

### 4. What is your targeted time to market (for a product on the market)

### 5. What is the business model

### 6. Who will be your first customer (do you have a first customer?)

### 7. Do you plan to raise funds through private investors ?

### 8. Have you already initiated contact with Venture Capital firms ?

## Technical Page 
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->

### Please provide answers to the following questions before the productisation meeting:

1. What is the status of your product development?

2. Are you aware of the eSSIF-Lab Library? What are the functionalities you would like to use?

### To be discussed duing the productisation meeting:

1. eSSIF-Lab partnerships: (see matchmaking process)
   1. what do you have on offer to other participants ?
   2. what would you like other participants to offer you ?

2. How does your product fit within the eSSIF-Lab vision and architecture ?

3. What is the timeline for the technical development ? Respect the milestones/timing… define the KPIs. 


