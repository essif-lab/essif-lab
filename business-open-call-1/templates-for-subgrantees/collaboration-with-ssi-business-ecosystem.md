# BOC-1 - Collaboration with SSI business ecosystem

## The needs of <!--our project-->

<!-- Describe what minimum inputs would you need from the other eSSIF-Lab subgrantees of eSSIF-Lab
- Please check the other eSSIF-Lab subgrantees’ solutions descriptions, e.g. to see what they can provide to your benefit.
- Please contact and communicate with other eSSIF-Lab subgrantees as you see fit.
- Specify the eSSIF-Lab subgrantees or other parties (EU-NGI, US-DHS-SVIP, cross-border, other) that you want to collaborate with, and that you are already working with.
- Identify the  eSSIF-Lab subgrantees and other parties would you want to perform interoperability tests with, and what the nature of such tests would be.
-->

## What <!--our project--> may provide

<!-- Advertise the things that you think may be of benefit to other subgrantees. Preferably, be concrete in your offers and specify the specific subgrantee(s) you target with that offer.
-->

## Anything else <!--our project--> needs to say

