# BOC-1 - Project Summary of <!Data as Currency>

For more information, please contact: 

<!--
- name of the person to contact
-ways to contact this person (email, git, slack, others...)
--> 

<!--Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others.

Be mindful of the audience you target, which include other eSSIF-Lab subgrantees, project experts that evaluate your work, and other audiences in the NGI-EU community. Make sure your texts help them understand how your work may benefit theirs, what you need of them, and how everything fits together-->

## Summary

<!--Provide a *concise* description of your subproject, that helps other subgrantees to quickly determine what value there is if they work together with you, and/or let their results interact and interop with your results.-->

## Business Problem

<!--Provide a *concise* description of the business problem you focus on. What is the problem? Who needs to deal with it/suffers drawbacks?-->

## (Technical) Solution

<!--Provide a *concise* list of the (concrete) results you will be providing (include a summary description of each of them), and how they (contribute to) solve the business problem(s)-->

## Integration

<!--Provide a precise description of how your results will fit with the eSSIF-Lab functional architecture (or modifications you need). Also, provide descriptions of how your results (are meant to) work, or link together in a meaningful way, both within the eSSIF-Lab family of projects, within EU-NGI, with US-DHS-SVIP, cross-border and/or other.-->

## <!--Your own section(s)-->

<!--Include sections for other stuff you want other eSSIF-Lab subgrantees (including those that applied for other calls) to become aware of.-->



